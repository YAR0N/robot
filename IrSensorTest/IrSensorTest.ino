#include <Robot.h>

int IRleft = A0;
int IRright = A3;

Robot robot;

void setup()
{
  Serial.begin(9600);
 
  robot.enableLeftIr(IRleft);
  robot.enableRightIr(IRright);
}

void loop()
{
  int right = robot.getRightIr().getCentimeters();
  int left = robot.getLeftIr().getCentimeters();
  
  Serial.print(left);
  Serial.print(" - ");
  Serial.println(right);
  
  delay(1000);
}
