//#define NDEBUG
#include <Robot.h>

int a1 = 8;
int a2 = 7;
int Apwm = 5;  

int b1 = 10;
int b2 = 9;
int Bpwm = 6;  

int IRleft = A0;
int IRright = A3;

const int DET = 80;

Robot robot;

const int SEARCH = 0;
const int ADJUSTMENT = 1;
const int ATTACK = 2;

int mode;

void setup()
{
  Serial.begin(9600);
  robot.enableMotorController(a1, a2, Apwm, b1, b2, Bpwm);
  robot.enableLeftIr(IRleft);
  robot.enableRightIr(IRright);
  
  mode = SEARCH; 
    
  delay(5000);
}

void search()
{
  //robot.getMotorController().stop();
  
  int left = robot.getLeftIr().getCentimeters();
  int right = robot.getRightIr().getCentimeters();
  
  if(left < DET && right < DET)
  {
    //robot.getMotorController().stop();
    mode = ATTACK;
#ifdef NDEBUG
    Serial.print(1);
    Serial.print(" - ");
    Serial.print(left);
    Serial.print(" - ");
    Serial.print(right);
    Serial.println("ATTACK");
#endif    
  }else{
    robot.getMotorController().left();
  }
}

void adjustment()
{
  int left = robot.getLeftIr().getCentimeters();
  int right = robot.getRightIr().getCentimeters();
  
  if(left < DET && right > left) {
    robot.getMotorController().right();
    mode = ATTACK;
#ifdef NDEBUG
    Serial.print(2);
    Serial.print(" - ");
    Serial.println("ATTACK");
#endif  
  } else if(left > DET && right < left){
    robot.getMotorController().left();
    mode = ATTACK;
#ifdef NDEBUG
    Serial.print(3);
    Serial.print(" - ");
    Serial.println("ATTACK");
#endif  
  } else {
    mode = SEARCH;
#ifdef NDEBUG
    Serial.print(4);
    Serial.print(" - ");
    Serial.println("SEARCH");
#endif  
  }
}

void attack()
{
  if (robot.getLeftIr().getCentimeters() < DET && robot.getRightIr().getCentimeters() < DET)
  {
    robot.getMotorController().forward();
  } else {
    mode = ADJUSTMENT;
#ifdef NDEBUG
    Serial.print(4);
    Serial.print(" - ");
    Serial.println("ADJUSTMENT");
#endif  
  }
}

void loop()
{
  switch(mode) {
    case SEARCH:
      search();
      break;
    case ADJUSTMENT:
      adjustment();
      break;
    case ATTACK:
      attack();
      break;
  }
}
