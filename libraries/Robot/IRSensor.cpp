#include "IRSensor.h"
#include <Arduino.h>

IRSensor::IRSensor(int dP):
	_distancePin(dP)
{	
	pinMode(_distancePin, INPUT);
	return;
}

IRSensor::~IRSensor()
{
	return;
}

const int IRSensor::getCentimeters() const
{
	int RawData = getRawData();
	/*
	RawData =  Cm
	314-210 = 15-30
	210-130 = 30-50
	130-100 = 50-70
	100-70  = 70-120
	*/

	if (RawData<=314 && RawData>=210){

		return 6026/RawData;

	}else if(RawData<210 && RawData>=130){

		return 6800/RawData;

	}else if(RawData<130 && RawData>=100){

		return 6900/RawData;

	}else if(RawData<100 && RawData>=70){

		return 8075/RawData;

	}else if(RawData < 70){
    	return  1000;
    } else {
        return -1;
    }
}

const int IRSensor::getRawData() const
{
    int RawData = 0;
    for (int i = 0; i < 10; ++i)
    {
        RawData += analogRead(_distancePin);
    }
    RawData = RawData/10;
	return RawData;
}