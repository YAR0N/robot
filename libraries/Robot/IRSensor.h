#ifndef IRSensor_H
#define IRSensor_H

class IRSensor
{
public:
	IRSensor(int);
	~IRSensor();

	const int getCentimeters() const;
	const int getRawData() const;

private:
	int _distancePin;
};

#endif