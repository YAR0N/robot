#include "MotorController.h"
#include <Arduino.h>

MotorController::MotorController(int a1, int a2, int Apwm, int b1, int b2, int Bpwm):
_A1(a1),
_A2(a2),
_Apwm(Apwm),
_B1(b1),
_B2(b2),
_Bpwm(Bpwm)
{
	return;
}

void MotorController::init()
{
	pinMode(_A1, OUTPUT);
  	pinMode(_A1, OUTPUT);
  	pinMode(_Apwm, OUTPUT);
  	pinMode(_B1, OUTPUT);
  	pinMode(_B2, OUTPUT);
  	pinMode(_Bpwm, OUTPUT);

  	_orientation = NORTH;
  	_speed = DEFAULT_SPEED;
}

void MotorController::changeOrientation()
{
	if (_orientation = NORTH){
		_orientation = SOUTH;
	} else {
		_orientation = NORTH;
	}
}

int MotorController::setSpeed(const int& speed)
{
	_speed = speed;
}

void MotorController::move(bool a1, bool b1, int speed) const
{
	if (_orientation == SOUTH)
	{
		a1 = !a1;
		b1 = !b1;
	}

	digitalWrite(_A1, a1);
    digitalWrite(_A2, !a1);
    analogWrite(_Apwm, speed);

    digitalWrite(_B1, b1);
    digitalWrite(_B2, !b1);
    analogWrite(_Bpwm, speed);
}

void MotorController::forward() const
{
	move(HIGH, HIGH, _speed);
}

void MotorController::backward() const
{
	move(LOW, LOW, _speed);
}

void MotorController::left() const
{
	move(HIGH, LOW, _speed);
}

void MotorController::right() const
{
	move(LOW, HIGH, _speed);
}

void MotorController::stop() const
{
	move(HIGH, HIGH, 0);
}