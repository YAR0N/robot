#ifndef MotorController_H
#define MotorController_H

class MotorController
{
public:
	MotorController(int a1, int a2, int Apwm, int b1, int b2, int Bpwm);
	~MotorController();

	void init();
	void changeOrientation();

	int setSpeed(const int& speed);

	void forward() const;
	void backward() const;
	void left() const;
	void right() const;
	void stop() const;
	
private:
	MotorController(const MotorController&);

	void move(bool a1, bool b1, int speed) const;

	int _orientation;

	int _speed;

	int _A1;
	int _A2;
	int _Apwm;

	int _B1;
	int _B2;
	int _Bpwm;

	static const int NORTH = 0;
	static const int SOUTH = 1;

	static const int DEFAULT_SPEED = 255;
};

#endif