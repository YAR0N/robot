#include "Robot.h"

Robot::Robot():
_mc(0),
_leftIR(0),
_rightIR(0)
{
	return;
}

Robot::~Robot()
{
	return;
}

void Robot::enableMotorController(int a1, int a2, int Apwm, int b1, int b2, int Bpwm)
{
	_mc = new MotorController(a1, a2, Apwm, b1, b2, Bpwm);
	_mc->init();
}

void Robot::enableLeftIr(int pin)
{
	_leftIR = new IRSensor(pin);
}

void Robot::enableRightIr(int pin)
{
	_rightIR = new IRSensor(pin);
}

const MotorController& Robot::getMotorController() const
{
	return *_mc;
}

const IRSensor& Robot::getLeftIr() const
{
	return *_leftIR;
}

const IRSensor& Robot::getRightIr() const
{
	return *_rightIR;
}