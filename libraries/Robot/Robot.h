#ifndef Robot_H
#define Robot_H

#include "MotorController.h"
#include "IRSensor.h"

class Robot
{
public:
	Robot();
	~Robot();

	const MotorController& getMotorController() const;
	const IRSensor& getLeftIr() const;
	const IRSensor& getRightIr() const;

	void enableMotorController(int a1, int a2, int Apwm, int b1, int b2, int Bpwm);

	void enableLeftIr(int pin);
	void enableRightIr(int pin);

private:

	MotorController* _mc;

	IRSensor* _leftIR;
	IRSensor* _rightIR;

};

#endif